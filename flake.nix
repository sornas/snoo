{
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nixpkgs.url = "nixpkgs";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = nixpkgs.legacyPackages.${system};
      in {
        packages.swim = pkgs.callPackage
          ({ lib, rustPlatform, fetchFromGitLab, nextpnr, yosys, cargo }:
            rustPlatform.buildRustPackage rec {
              pname = "swim";
              version = "0.0.0-3ed29f5a6cdbdfcc5cdf7a1479dac7137982781a";

              src = fetchFromGitLab {
                owner = "spade-lang";
                repo = "swim";
                rev = "2c58474284bc869db1cca6eaf540a37ad7198650";
                sha256 = "sha256-9A5b8XyjjKNG/dBZM6gKACv116wdwtSMgyMVQk5MTSQ=";
              };

              nativeBuildInputs = [ nextpnr yosys cargo ];

              cargoSha256 =
                "sha256-/wY/i0tfeQtgK1NYML9WAnMCqHPzMNVan40+aZs2goQ=";

              doCheck = false;

              meta = with lib; {
                description = "The toolchain for the language spade.";
                homepage = "https://gitlab.com/spade-lang/swim";
              };
            }) { };

        packages.spade-language-server = pkgs.callPackage
          ({ rustPlatform, fetchFromGitLab, lib }:
            rustPlatform.buildRustPackage rec {
              pname = "spade-language-server";
              version = "0.0.0-5eccf6c71724ec1074f69f535132a5b298d583ba";

              src = fetchFromGitLab {
                owner = "spade-lang";
                repo = "spade-language-server";
                rev = "5eccf6c71724ec1074f69f535132a5b298d583ba";
                sha256 = "sha256-HazZSqo+C70UZjhr0NNrDM/61Tw0vwP7Yaad+yvdmY4=";
              };

              cargoSha256 =
                "sha256-HcvgU/w0dDAqHtE6y7B2pdKCIU1xK0M5PwG0G3epKDU=";

              meta = with lib; {
                description =
                  "The language server for the spade programming language.";
                homepage =
                  "https://gitlab.com/spade-lang/spade-language-server";
              };
            }) { };

        devShells.default = pkgs.mkShell {
          nativeBuildInputs = [ pkgs.bashInteractive ];
          buildInputs = with self.packages.${system}; [
            swim
            pkgs.yosys
            pkgs.nextpnr
            pkgs.trellis
            pkgs.fujprog
            (pkgs.python310.withPackages (pyPkgs: [ pyPkgs.pillow ]))
            # spade-language-server
          ];
        };
      });
}
