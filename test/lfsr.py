# top=lfsr::next

from cocotb import triggers
from cocotb.clock import Clock

from spade import *

@cocotb.test()
async def ten_values_are_different(dut):
    s = SpadeExt(dut)
    clk = dut.clk_i
    await cocotb.start(Clock(clk, 1, units="ns").start())
    
    # Reset
    s.i.rst = "true"
    s.i.initial = "0"
    await triggers.ClockCycles(clk, 5)
    # Wait another half cycle for everything to align
    await FallingEdge(clk)
    s.i.rst = "false"

    seen = set()
    for _ in range(10):
        await FallingEdge(clk)
        n = s.o.value()
        if n in seen:
            assert False, "Value repeated"
        seen.add(n)
