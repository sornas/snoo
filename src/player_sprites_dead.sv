module e_player_dead_sprite_rom(input clk_i, input[0:0] i1_i, input[5:0] x1_i, input[5:0] y1_i, input[0:0] i2_i, input[5:0] x2_i, input[5:0] y2_i,output reg[47:0] __output );
logic[23:0] data[8191:0];
initial begin
$readmemh("src/player_sprites_dead.txt", data);end
always @(posedge clk_i) begin
   __output <= {data[i1_i * 64 * 64 + y1_i * 64 + x1_i], data[i2_i * 64 * 64 + y2_i * 64 + x2_i]};end
endmodule
