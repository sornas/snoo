import numpy as np
import imageio
import os
import matplotlib.pyplot as plt
import matplotlib


files = os.listdir(".")

for fname in files:
    if fname.endswith(".png"):
        image = imageio.imread(fname)
        alpha = image[:, :, 3]
        alpha[alpha != 0] = 255
        image[:, :, 3] = alpha
        imageio.imwrite("conv_" + fname, image)

