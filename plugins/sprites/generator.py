from PIL import Image
import sys
import os
import math

def main():
    assert len(sys.argv) > 4, "expected at least one input file one output spade, and one output verilog file"

    images = sys.argv[5:]

    verilog_filename = sys.argv[1]
    spade_filename = sys.argv[2]
    hex_filename = sys.argv[3]
    prefix = sys.argv[4]

    if os.path.exists(verilog_filename) and os.path.exists(spade_filename):
        needs_rebuild = False
        dependent_files = images + [__file__, "swim.toml"]
        target_time = os.path.getmtime(verilog_filename)
        needs_rebuild = any(map(
            lambda file: not os.path.exists(file) or os.path.getmtime(file) > target_time,
            dependent_files)
        )
    else:
        needs_rebuild = True


    if not needs_rebuild:
        return

    verilog = open(verilog_filename, 'w+')
    spade = open(spade_filename, 'w+')
    hexfile = open(hex_filename, 'w+')

    # Gather all the pixel data
    pixel_data = []

    for fname in images:
        fname = fname.strip()
        im = Image.open(fname)
        image = list(im.getdata())


        for y in range(64):
            for x in range(64):
                pixel = image[y * 64 + x]
                pixel_data.append(f"{pixel[0]:02x}{pixel[1]:02x}{pixel[2]:02x}")



    fn_name = f"{prefix}_sprite_rom" # leading dirs removed, no extension

    index_width = math.floor(math.log2(len(images)))+ 1

    spade.write("#[no_mangle]\n")
    spade.write(f"pipeline(1) {fn_name}(clk: clk, i1: int<{index_width}>, x1: int<6>, y1:int<6>, i2: int<{index_width}>, x2: int<6>, y2:int<6>) -> ((int<8>, int<8>, int<8>), (int<8>, int<8>, int<8>)) __builtin__")

    verilog.write(f"module e_{fn_name}(input clk_i, input[{index_width-1}:0] i1_i, input[5:0] x1_i, input[5:0] y1_i, input[{index_width-1}:0] i2_i, input[5:0] x2_i, input[5:0] y2_i,output reg[47:0] __output );\n")


    nearest_pow_2 = 2**(math.floor(math.log2(len(pixel_data))) + 1)

    verilog.write(f"logic[23:0] data[{nearest_pow_2 -1}:0];\n")

    for data in pixel_data:
        hexfile.write(f"{data}\n")

    for _ in range(0, nearest_pow_2 - len(pixel_data)):
        hexfile.write(f"{0:06x}\n")


    verilog.write("initial begin\n")
    verilog.write(f"$readmemh(\"{hex_filename}\", data);")
    verilog.write("end\n")




    verilog.write("always @(posedge clk_i) begin\n")
    verilog.write("   __output <= {data[i1_i * 64 * 64 + y1_i * 64 + x1_i], data[i2_i * 64 * 64 + y2_i * 64 + x2_i]};")
    verilog.write("end\n")

    verilog.write("endmodule\n")


        # f.write('entity ' + fn_name + '(clk: clk, x: int<6>, y: int<6>) -> (int<9>, int<9>, int<9>) {\n')

        # f.write('    let sprite: [int<25>; 256] = [\n')
        # for y in range(16):
        #     row = ''
        #     for x in range(16):
        #         pixel = image[y * 16 + x]
        #         row += f'        0x{pixel[0]:x}{pixel[1]:x}{pixel[2]:x},\n'
        #     row += '\n'
        #     f.write(row)
        # f.write('    ];\n')
        # # f.write('    let pixel: (int<9>, int<9>, int<9>) = sprite[std::conv::trunc(y)][std::conv::trunc(x)];\n')
        # f.write('   reg(clk) value = sprite[std::conv::zext((y << 6) + x)];\n')
        # f.write('    (trunc(value >> 16), trunc(value >> 8), trunc(value))\n')
        # # f.write('    (std::conv::trunc(pixel#0), std::conv::trunc(pixel#1), std::conv::trunc(pixel#2))\n')
        # f.write('}\n\n')
    verilog.close()

if __name__ == "__main__":
    main()
