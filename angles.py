#!/usr/bin/env python3

import math

angle_n = 4  # int<n>
d_n = 4

min_value = -(2 ** (angle_n - 1))
max_value = 2 ** (angle_n - 1) - 1

print("[")
for i in list(range(0, max_value + 1)) + list(range(min_value, 0)):
    angle = (i / (max_value + 1)) * 180
    # Revert sin and cos, since 0 deg is down
    dx = round(math.sin(math.radians(angle)) * (2 ** (d_n - 1) - 1))
    dy = round(math.cos(math.radians(angle)) * (2 ** (d_n - 1) - 1))
    print(f"    ({dx}, {dy}),")
print("]")
